#! /bin/bash

echo "Yahooo! I am prepared to build Yocto, Sir!"
echo "OK, what do I need to do?"
echo ""
echo "Uhmmm! Let me think... Let's start step by step"
echo ""
echo "a) "

RETVAL=0

cd ${WORKSPACE}/poky
if [ "$?" != "0" ]
then
	echo "Failed to enter poky directory"
	exit 1
fi

source oe-init-build-env

bitbake-layers add-layer ../meta-qcom
bitbake-layers add-layer ../meta-collabora-training

grep "INHERIT =" conf/local.conf
if [ "$?" != "0" ]
then
	echo "INHERIT = \"rm_work\"" >> conf/local.conf
	echo "BB_GENERATE_MIRROR_TARBALLS = \"1\"" >> conf/local.conf
	echo "DL_DIR = \"${WORKSPACE}/yocto-downloads\"" >> conf/local.conf
	echo "SSTATE_DIR = \"${WORKSPACE}/sstate-cache\"" >> conf/local.conf
	echo "RM_OLD_IMAGE = \"1\"" >> conf/local.conf
	echo "MACHINE = \"dragonboard-410c\"" >> conf/local.conf
	echo "PACKAGE_CLASSES = \"package_deb\"" >> conf/local.conf
fi

#grep "BBLAYERS +=" conf/bblayers.conf
#if [ "$?" != "0" ]
#then
#	echo "" >> conf/bblayers.conf
#	echo "BBLAYERS += \"$/poky/meta-intel \\ " >> conf/bblayers.conf
#	echo "             $/poky/meta-fsl-arm \\ " >> conf/bblayers.conf
#fi



echo "*** Building core image minimal ***"

bitbake core-image-minimal

exit $?
