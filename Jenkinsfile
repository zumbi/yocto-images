pipeline {
  options { timestamps() }
  agent { label 'docker-slave' }
  stages {
    stage ("checkout") {
      when { not { environment name: "gitlabBranch", value: null }}
      steps {
         /* Need manual checkout as checkout scm doesn't work with gitlab
          * configuration yet */
          script {
            if (env.gitlabTargetRepoSshURL == null) {
                env.gitlabTargetRepoSshURL = env.gitlabSourceRepoSshURL
            }
          }
          checkout changelog: true, poll: true, scm: [
            $class: 'GitSCM',
            branches: [[name: "source/${env.gitlabSourceBranch}"]],
            extensions: [[$class: 'PreBuildMerge',
                          options: [fastForwardMode: 'FF', mergeRemote: 'target', mergeStrategy: 'default', mergeTarget: "${env.gitlabTargetBranch}"]]],
            userRemoteConfigs: [ [ name: 'target', url: "${env.gitlabTargetRepoSshURL}", credentialsId: 'GITLABSSHKEY' ],
                                 [ name: 'source', url: "${env.gitlabSourceRepoSshURL}", credentialsId: 'GITLABSSHKEY' ]]
          ]
      }
    }
    stage("Setup Yocto") {
      steps {
        script {
          env.PIPELINE_VERSION = VersionNumber(
             versionNumberString: '${BUILD_DATE_FORMATTED,"yyyyMMdd"}.${BUILDS_TODAY_Z}')
          env.IMAGE = "${PIPELINE_VERSION}/yocto-image-${PIPELINE_VERSION}.img"
        }
        sh 'mkdir ${PIPELINE_VERSION}'
      }
    }

    stage("Build Yocto") {
      agent {
        dockerfile {
          label 'docker-slave'
          dir 'image-builder'
          args '--device=/dev/kvm'
          reuseNode true
        }
      }
      steps {
          sh 'echo CheckOut and Build Yocto'
          sh 'git clone http://git.yoctoproject.org/git/poky.git -b rocko'
          sh 'cd poky/'
          sh 'git clone https://github.com/ndechesne/meta-qcom -b rocko'
          sh '. oe-init-build-env && \
              bitbake-layers add-layer ../meta-qcom && \
              MACHINE=dragonboard-410c bitbake core-image-minimal'
          sh 'ls -laR build/tmp/deploy/images'
      }
    }
  }
  post {
    always {
        deleteDir()
    }
    success {
      script {
        if (env.gitlabBranch == null) {
          mattermostSend color: 'good',
                         message: ":tada: Build succeeded <${env.BUILD_URL}|${env.JOB_NAME} ${env.BUILD_NUMBER}>, Fresh images <https://images.collabora.co.uk/qua0009/${env.PIPELINE_VERSION}|here>",
                         channel: 'zumbi'
        }
      }
    }
    failure {
      script {
        if (env.gitlabBranch == null) {
          mattermostSend color: 'danger',
                         message: ":rage3: Build failed <${env.BUILD_URL}|${env.JOB_NAME} ${env.BUILD_NUMBER}>",
                         channel: 'zumbi'
        }
      }
    }
  }
}
